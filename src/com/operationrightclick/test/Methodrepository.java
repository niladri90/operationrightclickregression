package com.operationrightclick.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Methodrepository {

	static WebDriver driver;

	public static void appLaunch() {
		
		//Declaration of the set property
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");

	}

	public static void rightClick() {
		
		//Using Action class
		
		Actions action = new Actions(driver);
		WebElement rightClickElement=driver.findElement(By.cssSelector(".context-menu-one"));
		//WebElement rightClickElement=driver.findElement(By.className("hljs-function"));
		
        action.contextClick(rightClickElement).build().perform();
        WebElement getCopyText =driver.findElement(By.cssSelector(".context-menu-icon-copy"));
        //getText() method to get the text value
        String GetText = getCopyText.getText();
        //To print the value
        System.out.println(GetText);
        
        getCopyText.click();

	}
}
